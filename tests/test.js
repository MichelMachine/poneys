QUnit.test( "hello test", function( assert ) {
  assert.ok( 1 == "1", "Passed!" );
});

QUnit.test( "test parite", function( assert ) {
  assert.ok( estPair(8) == true, "Chouette!" );
});

QUnit.test( "test imparite", function( assert ) {
  assert.ok( estPair(3) == false, "Chouette!" );
});

QUnit.test( "test bonjour", function( assert ) {
  assert.ok( bonjour("Michel") == "bonjour Michel", "Chouette!" );
});

QUnit.test( "test RIB", function( assert ) {
  assert.ok( rib("11111","22222","abcd3333efg","11111 22222 abcd3333efg 42") == true, "Chouette!" );
});

QUnit.test( "fibonnaci", function( assert ) {
  assert.ok( fibonnaci("18") == "2584", "Chouette!" );
});

QUnit.test( "test secu", function( assert ) {
  assert.ok( identite_sociale("fille","93","02","75", "116", "344","2 93 02 75 116 344 02") == true, "Chouette!" );
});

QUnit.test( "test cle", function( assert ) {
  assert.ok( calculcle("2930275116344") == "02", "Chouette!" );
});
