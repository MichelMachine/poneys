<?php
  require_once 'src/Poneys.php';
  class PoneysTest extends \PHPUnit_Framework_TestCase {

protected $Poneys;

protected function setUp(){
      $this->Poneys = new Poneys();
	  $this->Poneys->setCount(8);
}    

protected function tearDown(){
	  unset($this->Poneys);
}    
public function test_removePoneyFromField() {
      // Setup
//      $this->Poneys = new Poneys();

      // Action
      $this->Poneys->removePoneyFromField(3);
      
      // Assert
      $this->assertEquals(5, $this->Poneys->getCount());
    }
    
public function test_putPoneyInField() {
      // Setup
//      $this->Poneys = new Poneys();

      // Action
//      $this->Poneys->addPoneyInField(1);
      $this->Poneys->addPoneyInField(1);
      
      // Assert
      $this->assertEquals(9, $this->Poneys->getCount());
    }

/**
* @expectedException Exception
*/
public function test_removeTooMuchPoneyFromField() {
      // Setup
//      $this->Poneys = new Poneys();

      // Action
      $this->Poneys->removePoneyFromField(9);
      
      // Assert
      $this->assertGreaterThanOrEqual(0, $this->Poneys->getCount());
    }

/**
* @dataProvider additionProvider
*/
public function test_removeMultipleValuesPoneyFromField($numberOfRemovedPoney) {
      // Setup	
//      $this->Poneys = new Poneys();

      // Action
      $this->Poneys->removePoneyFromField($numberOfRemovedPoney);
      
      // Assert
      $this->assertGreaterThanOrEqual(0, $this->Poneys->getCount());
    }

public function test_mocking(){
//je créé un mock pour ma classe Poney
	$stub = $this->getMockBuilder('Poneys')->getMock();

//je récupère la fonction getNames et je décide de la valeur de retour
	$stub->method('getNames')->willReturn('JohnnyLePoney');

// Assert
$this->assertEquals('JohnnyLePoney', $stub->getNames());
}

/**
* @dataProvider additionProvider
*/
public function test_availabality($numberOfPoney){
    // Setup
//    $this->Poneys = new Poneys();
	
    // Action
    $this->Poneys->addPoneyInField($numberOfPoney);
	
	$this->assertTrue($this->Poneys->isAvailable());	
}

public function additionProvider()
    {
        return array(
          array(0),
          array(1),
          array(3),
          array(5),
        );
    }
}
 ?>
