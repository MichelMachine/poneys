<?php 
  class Poneys {
      private $count = 8;
	  private $available = true;

      public function getCount() {
        return $this->count;
      }
      
	  public function setCount($value) {
        	$this->count=$value;
      }

      public function removePoneyFromField($number) {
			if(($this->count - $number)<0)
			{
				throw new Exception('too few poney');
			}
       		$this->count -= $number;
			if($this->count < 15)
				$this->available = true;
      }
      public function addPoneyInField($number) {

        $this->count += $number;
			if($this->count > 14)
				$this->available = false;
      }
      public function getNames () {

      
      }
      public function isAvailable() {
		return $this->available;
	  }
  }
?>
